1
00:00:00,240 --> 00:00:05,359
Okay, so we have our template HelloWorld.

2
00:00:03,040 --> 00:00:08,400
We go into that

3
00:00:05,359 --> 00:00:11,040
link and we copy/paste most of the code.

4
00:00:08,400 --> 00:00:13,200
So we want the code to actually create

5
00:00:11,040 --> 00:00:14,960
the file,

6
00:00:13,200 --> 00:00:16,240
and

7
00:00:14,960 --> 00:00:17,680
write that file until we close the

8
00:00:16,240 --> 00:00:18,960
HANDLE.

9
00:00:17,680 --> 00:00:21,840
So, we're going to move that into a

10
00:00:18,960 --> 00:00:21,840
function called

11
00:00:22,880 --> 00:00:27,800
WriteDataToFile.

12
00:00:23,920 --> 00:00:27,800


13
00:00:33,040 --> 00:00:37,920
We want to use the DisplayError

14
00:00:35,280 --> 00:00:37,920
function as well.

15
00:00:43,120 --> 00:00:46,640
In the DisplayError, there is a typo

16
00:00:45,200 --> 00:00:51,399
actually in their

17
00:00:46,640 --> 00:00:51,399
code, it's missing the ending bracket.

18
00:00:58,480 --> 00:01:04,640
So, the WriteDataToFile will have as an

19
00:01:01,039 --> 00:01:04,640
argument the file name,

20
00:01:07,840 --> 00:01:12,080
the

21
00:01:09,119 --> 00:01:13,040
data buffer,

22
00:01:12,080 --> 00:01:15,360
and the

23
00:01:13,040 --> 00:01:15,360
length.

24
00:01:25,360 --> 00:01:29,680
So, in the sample argv[1] is the file name.

25
00:01:30,400 --> 00:01:35,400
You can replace argv[1] with fileName.

26
00:01:44,799 --> 00:01:48,240
We don't need to test argv.

27
00:01:53,119 --> 00:01:56,159
And we want to pass the

28
00:01:55,040 --> 00:01:58,799
data,

29
00:01:56,159 --> 00:02:02,119
and the data to write length

30
00:01:58,799 --> 00:02:02,119
as an argument.

31
00:02:10,000 --> 00:02:13,879
So, we're going to call that function.

32
00:02:39,519 --> 00:02:43,599
So, now we have our data we want to write,

33
00:02:42,400 --> 00:02:46,640
the size,

34
00:02:43,599 --> 00:02:49,440
and we're calling WriteDataToFile,

35
00:02:46,640 --> 00:02:53,280
which is going to call CreateFile,

36
00:02:49,440 --> 00:02:54,720
WriteFile and then close the HANDLE.

37
00:02:53,280 --> 00:02:56,000
So, if we

38
00:02:54,720 --> 00:02:59,000
build that

39
00:02:56,000 --> 00:02:59,000
project,

40
00:03:07,920 --> 00:03:12,239
so we see it's missing one function used

41
00:03:10,480 --> 00:03:14,640
by DisplayError.

42
00:03:12,239 --> 00:03:16,319
So, if we go back to the actual sample, we

43
00:03:14,640 --> 00:03:19,840
see that we're missing

44
00:03:16,319 --> 00:03:19,840
probably includes.

45
00:03:33,040 --> 00:03:36,560
Okay,

46
00:03:33,920 --> 00:03:40,080
so we successfully built it.

47
00:03:36,560 --> 00:03:43,440
So now if we go to the target VM

48
00:03:40,080 --> 00:03:43,440
and run our hello,

49
00:03:45,040 --> 00:03:50,640
we see that it wrote a text file on the

50
00:03:47,840 --> 00:03:50,640
actual Desktop.

51
00:03:50,959 --> 00:03:54,640
If we rerun it,

52
00:03:53,120 --> 00:03:56,640
we see it's failing because the file

53
00:03:54,640 --> 00:03:58,239
already exists so we need to delete the

54
00:03:56,640 --> 00:04:01,560
file to re-run the

55
00:03:58,239 --> 00:04:01,560
actual sample.

56
00:04:01,840 --> 00:04:06,799
So, now let's debug our HelloWorld,

57
00:04:04,640 --> 00:04:10,439
writing a file on disk. We're going to

58
00:04:06,799 --> 00:04:10,439
first start Ghidra

59
00:04:14,400 --> 00:04:20,479
and we're going to load our DLLs and

60
00:04:16,799 --> 00:04:20,479
executable we want to analyze.

61
00:04:22,400 --> 00:04:27,280
Here, we just want to see

62
00:04:25,360 --> 00:04:29,120
the actual WriteFile happening both in

63
00:04:27,280 --> 00:04:32,520
userland and kernel, we're not actually

64
00:04:29,120 --> 00:04:32,520
loading Hello[World_lab].exe.

65
00:04:37,040 --> 00:04:42,400
We want to make sure the ret-sync plugin

66
00:04:38,639 --> 00:04:45,120
is started with Alt+S.

67
00:04:42,400 --> 00:04:45,120
That is listening.

68
00:04:46,400 --> 00:04:52,479
Now, let's start WinDbg.

69
00:04:49,520 --> 00:04:53,919
Remember, we have our

70
00:04:52,479 --> 00:04:57,919
batch script

71
00:04:53,919 --> 00:05:00,000
actually invoking the dbg-prep.cmd.

72
00:04:57,919 --> 00:05:02,479
And this dbg-prep.cmd will actually

73
00:05:00,000 --> 00:05:04,080
automatically synchronize with Ghidra.

74
00:05:02,479 --> 00:05:05,520
We don't need that breakpoint at the moment

75
00:05:04,080 --> 00:05:08,520
so we're just going to disable it for

76
00:05:05,520 --> 00:05:08,520
now.

77
00:05:18,320 --> 00:05:22,080
So, as you can see,

78
00:05:19,840 --> 00:05:24,479
it connected to the target VM, we're

79
00:05:22,080 --> 00:05:26,880
going to break, so it actually execute the

80
00:05:24,479 --> 00:05:28,400
command from the cmd.

81
00:05:26,880 --> 00:05:31,759
And now we see that it actually

82
00:05:28,400 --> 00:05:31,759
synchronized with the Ghidra.

83
00:05:34,479 --> 00:05:42,160
Okay, so we've built our binary

84
00:05:37,680 --> 00:05:42,160
Now, if we run it from the target VM,

85
00:05:43,120 --> 00:05:46,080
we'll see it's hanging.

86
00:05:49,280 --> 00:05:53,680
Actually, we got it wrong. We need to "hit a

87
00:05:51,280 --> 00:05:56,400
key to continue", to print it before

88
00:05:53,680 --> 00:05:56,400
getchar().

89
00:06:00,319 --> 00:06:03,759
So, now we have pushed our binary.

90
00:06:04,720 --> 00:06:11,280
It is telling us the binary is executed and

91
00:06:07,440 --> 00:06:11,280
it's telling us to hit a key to continue.

92
00:06:13,840 --> 00:06:16,800
We ran our binary,

93
00:06:18,560 --> 00:06:24,160
so now from WinDbg

94
00:06:21,360 --> 00:06:26,720
we can break,

95
00:06:24,160 --> 00:06:29,199
and we can find our actually HelloWorld[_lab.exe]

96
00:06:26,720 --> 00:06:29,199
process.

97
00:06:30,400 --> 00:06:34,880
So, we get the result which is a _KPROCESS.

98
00:06:32,960 --> 00:06:38,080


99
00:06:34,880 --> 00:06:38,080
So, the next step is to

100
00:06:38,160 --> 00:06:43,919
actually change the context to the

101
00:06:40,160 --> 00:06:43,919
actual process we're interested in.

102
00:06:44,639 --> 00:06:49,840
Then, we need to hit go to switch to the

103
00:06:47,199 --> 00:06:49,840
actual [process] context.

104
00:06:51,120 --> 00:06:57,199
Now, we can use the !process command

105
00:06:53,599 --> 00:06:57,199
to see the information for the process.

106
00:06:58,319 --> 00:07:02,639
We see it has only one thread,

107
00:07:00,800 --> 00:07:05,039
and

108
00:07:02,639 --> 00:07:07,440
it's actually the HelloWorld executable

109
00:07:05,039 --> 00:07:09,360
we're interested in.

110
00:07:07,440 --> 00:07:11,919
The next step is to reload the symbols

111
00:07:09,360 --> 00:07:14,080
for all the loaded modules. So, now the

112
00:07:11,919 --> 00:07:16,720
modules have been reloaded, we can now

113
00:07:14,080 --> 00:07:18,880
set a breakpoint onto the kernel32

114
00:07:16,720 --> 00:07:22,080
WriteFile function,

115
00:07:18,880 --> 00:07:22,080
and then continue execution.

116
00:07:28,080 --> 00:07:31,720
To go to our VM,

117
00:07:31,919 --> 00:07:36,800
we see that it's actually black now

118
00:07:33,840 --> 00:07:38,960
because probably a breakpoint hit.

119
00:07:36,800 --> 00:07:40,560
So, we can see a breakpoint already hit

120
00:07:38,960 --> 00:07:44,360
but it's not actually from HelloWorld,

121
00:07:40,560 --> 00:07:44,360
so we can continue execution.

122
00:07:51,919 --> 00:07:55,680
If that happens again

123
00:07:54,720 --> 00:07:59,840
too much,

124
00:07:55,680 --> 00:07:59,840
you can just disable the breakpoint.

125
00:08:05,199 --> 00:08:09,520
So, we're just going to temporarily

126
00:08:06,479 --> 00:08:09,520
disable the breakpoint,

127
00:08:10,240 --> 00:08:14,400
just to get access to our target VM

128
00:08:12,080 --> 00:08:14,400
again.

129
00:08:14,800 --> 00:08:17,360
And now,

130
00:08:21,199 --> 00:08:27,400
we should be able to set the

131
00:08:23,680 --> 00:08:27,400
the breakpoint again.

132
00:08:28,000 --> 00:08:32,280
So, let's re-enable our breakpoint.

133
00:08:40,640 --> 00:08:45,519
So now, we see we hit the breakpoint,

134
00:08:43,760 --> 00:08:48,959
and as you can see we are in the

135
00:08:45,519 --> 00:08:51,440
HelloWorld_lab.exe context.

136
00:08:48,959 --> 00:08:54,600
So, if we reload

137
00:08:51,440 --> 00:08:54,600
the symbols,

138
00:08:56,240 --> 00:09:00,240
so now let's

139
00:08:58,240 --> 00:09:01,760
check the backtrace,

140
00:09:00,240 --> 00:09:03,120
so we don't manage to get a better

141
00:09:01,760 --> 00:09:05,680
backtrace but I guess it's because

142
00:09:03,120 --> 00:09:08,240
HelloWorld is not actually loaded into WinDbg.

143
00:09:05,680 --> 00:09:09,760
We don't have the symbols and stuff. The

144
00:09:08,240 --> 00:09:11,680
whole point is to see that it's actually

145
00:09:09,760 --> 00:09:13,519
calling kernel32

146
00:09:11,680 --> 00:09:15,519
WriteFile. So, the next step is to

147
00:09:13,519 --> 00:09:18,080
actually set a breakpoint on the kernel

148
00:09:15,519 --> 00:09:18,080
syscall.

149
00:09:19,600 --> 00:09:24,880
We can actually

150
00:09:21,360 --> 00:09:24,880
disable the first breakpoint.

151
00:09:27,760 --> 00:09:31,360
And now, if we continue execution we

152
00:09:29,200 --> 00:09:33,760
should hit the syscall. Before we do that we

153
00:09:31,360 --> 00:09:35,839
can see first that in Ghidra,

154
00:09:33,760 --> 00:09:38,160
we are at the moment

155
00:09:35,839 --> 00:09:40,000
actually

156
00:09:38,160 --> 00:09:42,480
connected.

157
00:09:40,000 --> 00:09:45,440
So, if we do,

158
00:09:42,480 --> 00:09:45,440
and we step over

159
00:09:46,240 --> 00:09:51,800
you can see we are going into the

160
00:09:48,800 --> 00:09:51,800
function.

161
00:09:52,160 --> 00:09:57,600
And so if we set a breakpoint

162
00:09:54,640 --> 00:09:57,600
later in the function

163
00:09:59,360 --> 00:10:04,399
with alt-F3,

164
00:10:01,200 --> 00:10:07,600
and continue execution,

165
00:10:04,399 --> 00:10:07,600
we're going to hit the breakpoint.

166
00:10:11,279 --> 00:10:15,519
We see we are on the test

167
00:10:12,959 --> 00:10:16,480
if lpOverlapped.

168
00:10:15,519 --> 00:10:19,480
So, we're going to disable the

169
00:10:16,480 --> 00:10:19,480
breakpoint,

170
00:10:19,680 --> 00:10:23,040
and we're going to continue execution.

171
00:10:28,079 --> 00:10:32,800
As you can see we hit the syscall in

172
00:10:30,399 --> 00:10:34,560
kernel so here we are back to

173
00:10:32,800 --> 00:10:36,000
ntoskrnl,

174
00:10:34,560 --> 00:10:39,200
and

175
00:10:36,000 --> 00:10:39,200
we look at the backtrace.

176
00:10:39,839 --> 00:10:45,760
We can see that we went from HelloWorld,

177
00:10:43,040 --> 00:10:48,320
to kernelbase!WriteFile

178
00:10:45,760 --> 00:10:49,600
then into ntdll

179
00:10:48,320 --> 00:10:52,959
then

180
00:10:49,600 --> 00:10:56,800
go into KiSystemServiceCopyEnd that

181
00:10:52,959 --> 00:10:56,800
ends up calling the actual syscall.

182
00:10:57,360 --> 00:11:02,480
If we disable the breakpoints,

183
00:10:59,760 --> 00:11:04,640
and just step over

184
00:11:02,480 --> 00:11:06,320
we'll be able to step over

185
00:11:04,640 --> 00:11:08,480
in Ghidra as well,

186
00:11:06,320 --> 00:11:10,320
and we can analyze

187
00:11:08,480 --> 00:11:13,120
the actual function.

188
00:11:10,320 --> 00:11:14,880
For instance, we can do Alt+F3

189
00:11:13,120 --> 00:11:16,320
here

190
00:11:14,880 --> 00:11:19,279
in Ghidra and it's going to set a

191
00:11:16,320 --> 00:11:21,760
breakpoint automatically in WinDbg. We can

192
00:11:19,279 --> 00:11:24,320
continue execution

193
00:11:21,760 --> 00:11:24,320
and just see

194
00:11:25,440 --> 00:11:31,440
it hits the actual code.

195
00:11:29,120 --> 00:11:34,399
Okay, I think we managed to reach our

196
00:11:31,440 --> 00:11:37,200
goal here, I hope you had fun

197
00:11:34,399 --> 00:11:40,800
and let's use that to debug interesting

198
00:11:37,200 --> 00:11:40,800
stuff from now on. Thank you!

