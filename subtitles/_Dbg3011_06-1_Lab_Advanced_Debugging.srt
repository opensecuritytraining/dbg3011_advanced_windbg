﻿1
00:00:00,080 --> 00:00:04,720
Okay, in this part it's going to be you

2
00:00:02,399 --> 00:00:07,440
that are going to do the work. So the

3
00:00:04,720 --> 00:00:09,360
goal is to write some code in order to

4
00:00:07,440 --> 00:00:10,960
write some data into a file. You're going

5
00:00:09,360 --> 00:00:13,360
to have to modify the helloworld

6
00:00:10,960 --> 00:00:15,519
project and call functions like

7
00:00:13,360 --> 00:00:17,199
CreateFile to open a file and WriteFile to

8
00:00:15,519 --> 00:00:19,680
write content into a file. You can use

9
00:00:17,199 --> 00:00:21,520
this MSDN resource which has some code

10
00:00:19,680 --> 00:00:23,039
to help you. Once you've done that and

11
00:00:21,520 --> 00:00:26,160
you have pushed the binary onto the

12
00:00:23,039 --> 00:00:29,279
target VM, you can try to debug it with

13
00:00:26,160 --> 00:00:31,119
both WinDbg and Ghidra, and analyze what

14
00:00:29,279 --> 00:00:33,200
is happening in the debugger as well as

15
00:00:31,119 --> 00:00:34,000
the disassembler. Let's do it. So to help

16
00:00:33,200 --> 00:00:36,399
you,

17
00:00:34,000 --> 00:00:38,239
you can use this

18
00:00:36,399 --> 00:00:41,120
link here. You can see there is an

19
00:00:38,239 --> 00:00:44,160
example to open a file for writing

20
00:00:41,120 --> 00:00:45,680
and it uses CreateFile and WriteFile.

21
00:00:44,160 --> 00:00:47,200
And there's the main function with some

22
00:00:45,680 --> 00:00:49,039
data

23
00:00:47,200 --> 00:00:51,039
then it's calling CreateFile with some

24
00:00:49,039 --> 00:00:53,840
arguments with error checking then it's

25
00:00:51,039 --> 00:00:55,920
writing the data into the file

26
00:00:53,840 --> 00:00:58,559
and at the end, it's closing the HANDLE

27
00:00:55,920 --> 00:01:00,480
to the file. It also has

28
00:00:58,559 --> 00:01:03,600
helpers function

29
00:01:00,480 --> 00:01:05,360
for debugging problems.

30
00:01:03,600 --> 00:01:07,760
The idea is you're going to go into that

31
00:01:05,360 --> 00:01:09,520
lab and

32
00:01:07,760 --> 00:01:11,280
add missing functions calls here to

33
00:01:09,520 --> 00:01:13,760
write some data into the file. It's your

34
00:01:11,280 --> 00:01:13,760
turn now.

