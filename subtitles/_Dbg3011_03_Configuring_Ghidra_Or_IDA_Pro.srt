1
00:00:00,240 --> 00:00:04,000
Hi everyone, in this part we're going to

2
00:00:02,240 --> 00:00:06,720
show you how to configure your

3
00:00:04,000 --> 00:00:08,480
disassembler. It can be Ghidra or IDA.

4
00:00:06,720 --> 00:00:10,400
Let's get started. So, in this course

5
00:00:08,480 --> 00:00:13,280
we're going to mainly use Ghidra which

6
00:00:10,400 --> 00:00:15,120
has both, a disassembler and a decompiler.

7
00:00:13,280 --> 00:00:17,440
And to be fair, it's the best alternative

8
00:00:15,120 --> 00:00:19,439
to IDA Pro being like free alternative

9
00:00:17,440 --> 00:00:21,439
because it has a decompiler on top of

10
00:00:19,439 --> 00:00:23,439
the disassembler, all that being free. The

11
00:00:21,439 --> 00:00:25,920
decompiler is not as good as IDA Pro,

12
00:00:23,439 --> 00:00:28,080
because it actually requires a lot more

13
00:00:25,920 --> 00:00:29,840
manual work, when you're actually reverse

14
00:00:28,080 --> 00:00:31,519
engineering but it's still better than

15
00:00:29,840 --> 00:00:33,360
not having any decompiler at all. So, we

16
00:00:31,519 --> 00:00:35,440
already installed Ghidra when we

17
00:00:33,360 --> 00:00:38,800
initially configured the virtual machine.

18
00:00:35,440 --> 00:00:40,800
So, Ghidra has this concept of project.

19
00:00:38,800 --> 00:00:42,320
When you create a project, you can have

20
00:00:40,800 --> 00:00:44,719
several binaries you want to reverse

21
00:00:42,320 --> 00:00:47,039
engineer or analyze inside that project

22
00:00:44,719 --> 00:00:50,000
and so for a given project Ghidra will

23
00:00:47,039 --> 00:00:52,320
let you use tools on the actual binaries

24
00:00:50,000 --> 00:00:54,480
and so one of them is called the code

25
00:00:52,320 --> 00:00:56,480
browser and this is the main tool we're

26
00:00:54,480 --> 00:00:58,640
going to use on binaries. When we analyze

27
00:00:56,480 --> 00:01:00,719
binaries, you can either open the

28
00:00:58,640 --> 00:01:03,039
binaries into different CodeBrowsers or

29
00:01:00,719 --> 00:01:04,960
into the same CodeBrowser. We'll see

30
00:01:03,039 --> 00:01:07,200
later that when we use ret-sync we

31
00:01:04,960 --> 00:01:09,360
actually want all the binaries to be

32
00:01:07,200 --> 00:01:11,680
open in the same CodeBrowser. So if

33
00:01:09,360 --> 00:01:14,080
you get an actual archived project, you

34
00:01:11,680 --> 00:01:16,320
can open Ghidra and then import the

35
00:01:14,080 --> 00:01:19,799
archived project. It will actually create

36
00:01:16,320 --> 00:01:21,600
one project file .gpr along with a

37
00:01:19,799 --> 00:01:23,680
directory .rep. And the directory

38
00:01:21,600 --> 00:01:26,479
contains all the project files. This

39
00:01:23,680 --> 00:01:28,400
directory will be useful when we add

40
00:01:26,479 --> 00:01:30,000
the ret-sync plugin because we're going

41
00:01:28,400 --> 00:01:32,000
to have to create files inside that

42
00:01:30,000 --> 00:01:33,840
folder. So, this is what Ghidra looks like,

43
00:01:32,000 --> 00:01:35,759
we can see on the left the disassembler

44
00:01:33,840 --> 00:01:38,799
on the right the decompiler. There are

45
00:01:35,759 --> 00:01:40,799
interesting shortcuts Ctrl+B and Ctrl+D

46
00:01:38,799 --> 00:01:43,119
that allow you to show existing

47
00:01:40,799 --> 00:01:45,040
bookmarks and create new bookmarks. For

48
00:01:43,119 --> 00:01:47,200
the project we generally give you, we'll

49
00:01:45,040 --> 00:01:49,600
have created some bookmarks so you can

50
00:01:47,200 --> 00:01:52,240
easily find things. And at the bottom, you

51
00:01:49,600 --> 00:01:53,920
see on the screenshot the bookmark tab

52
00:01:52,240 --> 00:01:56,159
and you see that the one on the top has

53
00:01:53,920 --> 00:01:59,040
been created by us and you can actually

54
00:01:56,159 --> 00:02:01,200
sort all the bookmarks by category and

55
00:01:59,040 --> 00:02:03,520
the one that don't have any category are

56
00:02:01,200 --> 00:02:05,920
the custom ones. The last shortcut that

57
00:02:03,520 --> 00:02:08,160
is useful is the ret-sync extension. In

58
00:02:05,920 --> 00:02:11,280
order to start that extension, you would

59
00:02:08,160 --> 00:02:14,000
use Alt+S. These are a couple of Ghidra

60
00:02:11,280 --> 00:02:15,360
tricks, we want to configure on Ghidra

61
00:02:14,000 --> 00:02:17,520
that we'll show you in a minute. One of

62
00:02:15,360 --> 00:02:19,840
them is to actually change the decimal

63
00:02:17,520 --> 00:02:22,000
numbers into hexadecimal numbers. Another

64
00:02:19,840 --> 00:02:24,480
one is to actually highlight similar

65
00:02:22,000 --> 00:02:26,319
symbols into the window with just left

66
00:02:24,480 --> 00:02:28,800
click. Another one is to replace the

67
00:02:26,319 --> 00:02:31,120
names by registers. And the last one is

68
00:02:28,800 --> 00:02:32,560
to actually hide bytes into the listing

69
00:02:31,120 --> 00:02:34,879
window. If you don't want to use Ghidra

70
00:02:32,560 --> 00:02:37,599
and you prefer using IDA Pro, we don't

71
00:02:34,879 --> 00:02:40,000
provide an actual project for Ghidra [->IDA] but

72
00:02:37,599 --> 00:02:42,000
you can feel free to port the Ghidra

73
00:02:40,000 --> 00:02:44,160
annotations to IDA. We recommend using

74
00:02:42,000 --> 00:02:47,280
it only if you have a decompiler either

75
00:02:44,160 --> 00:02:49,840
using the license IDA Pro or using the

76
00:02:47,280 --> 00:02:51,680
free version with the online decompiler.

77
00:02:49,840 --> 00:02:54,080
So, in the debugger VM you should have

78
00:02:51,680 --> 00:02:55,680
Ghidra installed into tools\ghidra

79
00:02:54,080 --> 00:03:00,360
with the version. If you double click on

80
00:02:55,680 --> 00:03:00,360
ghidraRun.bat, it will start Ghidra.

81
00:03:10,640 --> 00:03:15,519
By default, it will show you this window,

82
00:03:13,519 --> 00:03:18,519
you see there is no open project at the

83
00:03:15,519 --> 00:03:18,519
moment.

84
00:03:20,080 --> 00:03:23,519
You should have been provided

85
00:03:22,159 --> 00:03:25,360
the hello_world

86
00:03:23,519 --> 00:03:28,560
Ghidra project.

87
00:03:25,360 --> 00:03:31,519
It's an archived project at the moment,

88
00:03:28,560 --> 00:03:34,159
we're gonna go into File,

89
00:03:31,519 --> 00:03:38,480
Restore Project...

90
00:03:34,159 --> 00:03:38,480
and then look for the archived file.

91
00:03:44,959 --> 00:03:49,680
We're going to name our project,

92
00:03:47,040 --> 00:03:49,680
"hello_world".

93
00:03:52,159 --> 00:03:58,159
We see it created a Ghidra project file

94
00:03:55,200 --> 00:04:01,439
as well as a .rep folder with the

95
00:03:58,159 --> 00:04:03,519
actual Ghidra project information.

96
00:04:01,439 --> 00:04:06,159
We also see that three binaries are part

97
00:04:03,519 --> 00:04:08,000
of the project,

98
00:04:06,159 --> 00:04:11,599
this is the CodeBrowser one, so we can

99
00:04:08,000 --> 00:04:14,319
either open it by clicking on this

100
00:04:11,599 --> 00:04:16,799
or on an actual binary. Let's open

101
00:04:14,319 --> 00:04:19,799
kernelBase.dll, we double click on

102
00:04:16,799 --> 00:04:19,799
KernelBase.dll,

103
00:04:22,960 --> 00:04:27,280
we can see

104
00:04:24,720 --> 00:04:29,840
the disassembler window

105
00:04:27,280 --> 00:04:31,440
as well as the decompiler window. Since

106
00:04:29,840 --> 00:04:34,800
we're not in a function, nothing is shown

107
00:04:31,440 --> 00:04:34,800
into the decompiler window,

108
00:04:35,120 --> 00:04:38,479
so in window we see Bookmarks, we can use

109
00:04:37,199 --> 00:04:41,759
Ctrl+B

110
00:04:38,479 --> 00:04:44,240
to open the Bookmarks tab.

111
00:04:41,759 --> 00:04:46,479
At the bottom, we see the Bookmarks tab

112
00:04:44,240 --> 00:04:48,400
and if we click on Category,

113
00:04:46,479 --> 00:04:49,600
we see that one of them doesn't have

114
00:04:48,400 --> 00:04:53,280
anything

115
00:04:49,600 --> 00:04:54,960
it's an actual bookmark we created.

116
00:04:53,280 --> 00:04:57,120
We see it corresponds to the WriteFile

117
00:04:54,960 --> 00:04:58,960
function.

118
00:04:57,120 --> 00:05:01,120
There are a few things I don't like in

119
00:04:58,960 --> 00:05:03,680
Ghidra by default. For instance,

120
00:05:01,120 --> 00:05:06,000
if we click on hFile, I'd like all the

121
00:05:03,680 --> 00:05:07,759
references of the same variable to be

122
00:05:06,000 --> 00:05:10,160
highlighted automatically, it's not the

123
00:05:07,759 --> 00:05:12,400
case. Also, I don't necessarily need to

124
00:05:10,160 --> 00:05:13,840
see the bytes on the disassembler window

125
00:05:12,400 --> 00:05:17,120
by default.

126
00:05:13,840 --> 00:05:20,560
And the last thing is the registers

127
00:05:17,120 --> 00:05:22,720
in the disassembly window actually named

128
00:05:20,560 --> 00:05:25,360
to the actual variable and I find it

129
00:05:22,720 --> 00:05:26,400
hard to read. So, to change all these

130
00:05:25,360 --> 00:05:28,080
defaults,

131
00:05:26,400 --> 00:05:29,440
I go into Edit,

132
00:05:28,080 --> 00:05:32,880
Tool Options...,

133
00:05:29,440 --> 00:05:35,840
then I go into Editors,

134
00:05:32,880 --> 00:05:35,840
Structure Editors,

135
00:05:36,160 --> 00:05:41,600
I can change "Show the numbers in

136
00:05:38,960 --> 00:05:46,320
hexadecimal", in order to change that from

137
00:05:41,600 --> 00:05:46,320
decimal. Then I go into Listing Fields,

138
00:05:46,639 --> 00:05:51,440
Cursor Text Highlights,

139
00:05:49,280 --> 00:05:54,080
and I change the Mouse Button To

140
00:05:51,440 --> 00:05:56,960
Activate to Left. Finally, I go into

141
00:05:54,080 --> 00:05:56,960
Operand Fields,

142
00:05:57,840 --> 00:06:01,919
and I untick Markup Register Variables

143
00:06:00,720 --> 00:06:05,199
References,

144
00:06:01,919 --> 00:06:05,199
then I save it.

145
00:06:05,680 --> 00:06:09,280
As you can see the registers are now

146
00:06:07,520 --> 00:06:11,440
shown instead of the actual variable

147
00:06:09,280 --> 00:06:13,520
names. Also if I click on hFile, all the

148
00:06:11,440 --> 00:06:15,600
references are shown. The last thing I

149
00:06:13,520 --> 00:06:16,560
want to do is I want to remove all the

150
00:06:15,600 --> 00:06:18,400
bytes

151
00:06:16,560 --> 00:06:21,759
that are present here.

152
00:06:18,400 --> 00:06:23,600
So, to do so I go into the disassembler

153
00:06:21,759 --> 00:06:25,360
window and I click Edit the Listing

154
00:06:23,600 --> 00:06:28,240
fields.

155
00:06:25,360 --> 00:06:30,000
As you can see, the address is this, the

156
00:06:28,240 --> 00:06:32,400
bytes are this,

157
00:06:30,000 --> 00:06:33,919
and the mnemonics are this one. So what I

158
00:06:32,400 --> 00:06:36,080
want to do is I want to remove the bytes.

159
00:06:33,919 --> 00:06:39,039
So, I right-click on Bytes,

160
00:06:36,080 --> 00:06:40,639
and then Disable Field,

161
00:06:39,039 --> 00:06:43,680
and then I don't see the bytes anymore.

162
00:06:40,639 --> 00:06:43,680
Okay, we're good to go.

